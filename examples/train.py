"""Run linear regression example."""
import math

import models.linear_regression as linear_model
import torch

import nn_helper
from nn_helper.callbacks.verbose_logger import VerboseLogger


def run_model() -> None:
    """Run training."""
    # Create Tensors to hold input and outputs.
    x = torch.linspace(-math.pi, math.pi, 2000)
    y = torch.sin(x)

    p = torch.tensor([1, 2, 3])
    xx = x.unsqueeze(-1).pow(p)

    # Only 1 sample. Hence, batch size of 1.
    data_loader = torch.utils.data.DataLoader(dataset=linear_model.Dataset(x=xx, y=y), batch_size=1)

    loss_fn = torch.nn.MSELoss()

    model = linear_model.LinearRegression()

    optimizer = torch.optim.Adam(params=model.parameters(), lr=1e-2)

    # Train the model with 10 epochs and use
    # Verbose Logger to put metrics to terminal.
    nn_helper.fit(
        model=model,
        data_loader=data_loader,
        epochs=10,
        loss_fn=loss_fn,
        optimizer=optimizer,
        callbacks=[VerboseLogger()],
    )


if __name__ == "__main__":
    run_model()
