"""A Linear Regression Model."""
import torch

import nn_helper


class LinearRegression(nn_helper.ModelBase):
    """Simple linear regression model."""

    def __init__(self):
        """Initialise instance."""
        super().__init__()
        self.linear = torch.nn.Linear(3, 1)
        self.flatten = torch.nn.Flatten(0, 1)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Forward pass through model.

        Args:
            x (torch.Tensor): Forward pass data.

        Returns:
            torch.Tensor: Output of the model function.
        """
        x = self.linear(x)
        return self.flatten(x)
