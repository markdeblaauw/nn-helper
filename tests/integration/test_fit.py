import math
import unittest
from tempfile import TemporaryDirectory

import torch

import nn_helper
import nn_helper.callbacks as callbacks
from tests.test_model.dataset import Dataset
from tests.test_model.model import LinearRegression


class TestTrainModel(unittest.TestCase):
    def setUp(self) -> None:
        # Create Tensors to hold input and outputs.
        x = torch.linspace(-math.pi, math.pi, 20)
        y = torch.sin(x)

        p = torch.tensor([1, 2, 3])
        xx = x.unsqueeze(-1).pow(p)

        # Only 1 sample. Hence, batch size of 1.
        self.data_loader = torch.utils.data.DataLoader(dataset=Dataset(x=xx, y=y), batch_size=1)

        self.loss_fn = torch.nn.MSELoss()

        self.model = LinearRegression()

        self.optimizer = torch.optim.Adam(params=self.model.parameters(), lr=1e-2)

    def test_train_model_lean(self):
        nn_helper.fit(
            model=self.model,
            data_loader=self.data_loader,
            epochs=2,
            loss_fn=self.loss_fn,
            optimizer=self.optimizer,
        )

    def test_train_model_with_verbose_save_and_lr_callbacks(self):
        with TemporaryDirectory() as tmp_model, TemporaryDirectory() as tmp_checkpoint:
            nn_helper.fit(
                model=self.model,
                data_loader=self.data_loader,
                epochs=2,
                loss_fn=self.loss_fn,
                optimizer=self.optimizer,
                callbacks=[
                    callbacks.VerboseLogger(),
                    callbacks.SaveModel(model_path=tmp_model, checkpoint_path=tmp_checkpoint),
                    callbacks.LRBatchDecay(lr=1e-2, lr_decay=1e-5),
                ],
            )
