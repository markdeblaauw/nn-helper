"""Logic to create a data loader."""
from typing import Tuple

import torch
import torch.utils.data as data


class Dataset(data.Dataset):
    """A simple Pytorch Dataset class for a DataLoader."""

    def __init__(self, x: torch.Tensor, y: torch.Tensor):
        """Initialise the Dataset with train data and target data.

        Args:
            x (torch.Tensor): The train data.
            y (torch.Tensor): The target data
        """
        self.x = x
        self.y = y

    def __getitem__(self, index: int) -> Tuple[torch.Tensor, torch.Tensor]:
        """Samples train and target data.

        Args:
            index (int): Which sample to take.

        Returns:
            Tuple[torch.Tensor, torch.Tensor]: Train and target sample.
        """
        return self.x[index], self.y[index]

    def __len__(self) -> None:
        """Getter for number of samples in training data."""
        return len(self.x)
