"""Loss functions for linear regression models."""
import torch


class LossFunction(torch.nn.Module):
    """A linear regression example loss function."""

    def __init__(self) -> None:
        """Initialise the loss class."""
        super().__init__()
        self.loss_fn = torch.nn.MSELoss()

    def forward(self, x: torch.Tensor, y: torch.Tensor) -> float:
        """Forward pass through loss function.

        Args:
            x (torch.Tensor): Training data model function output.
            y (torch.Tensor): Labels.

        Returns:
            float: The loss value.
        """
        return self.loss_fn(x, y)
