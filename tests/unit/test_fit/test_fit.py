import unittest
from unittest import mock
from unittest.mock import MagicMock, patch

import torch

from nn_helper.base.model import ModelBase
from nn_helper.train import fit


class TestFit(unittest.TestCase):
    def test_model_assert_wrong_instance(self):
        with patch("nn_helper.train.CallbackList") as mock_callback_list:
            mock_model = MagicMock()
            mock_model.prepare_batch.return_value = (1, 2)

            mock_data_loader = MagicMock()
            mock_data_loader.__iter__.return_value = ["1"]
            mock_data_loader.__len__.return_value = 1
            mock_data_loader.batch_size = 1

            mock_loss_fn = MagicMock()
            mock_optimizer = MagicMock()

            self.assertRaises(
                AssertionError,
                fit,
                model=mock_model,
                data_loader=mock_data_loader,
                epochs=1,
                loss_fn=mock_loss_fn,
                optimizer=mock_optimizer,
            )

    def test_fit_calls_for_one_run(self):
        with patch("nn_helper.train.CallbackList") as mock_callback_list, patch(
            "nn_helper.train.MetricAggregator"
        ) as mock_metric_aggregator:
            mock_model = MagicMock(spec=ModelBase)
            mock_model.prepare_batch.return_value = (1, 2)

            mock_data_loader = MagicMock()
            mock_data_loader.__iter__.return_value = ["1"]
            mock_data_loader.__len__.return_value = 1
            mock_data_loader.batch_size = 1

            mock_loss_fn = MagicMock()
            mock_optimizer = MagicMock()

            fit(
                model=mock_model,
                data_loader=mock_data_loader,
                epochs=1,
                loss_fn=mock_loss_fn,
                optimizer=mock_optimizer,
            )

            mock_callback_list_calls = mock_callback_list.mock_calls
            self.assertEqual(mock_callback_list_calls[0], mock.call([mock_metric_aggregator.return_value]))
            self.assertEqual(mock_callback_list_calls[1], mock.call().set_model(mock_model))
            self.assertEqual(
                mock_callback_list_calls[2],
                mock.call().set_params(
                    {
                        "optimizer": mock_optimizer,
                        "num_batches": 1,
                        "batch_size": 1,
                        "metrics": [],
                        "verbose": 1,
                    }
                ),
            )
            self.assertEqual(mock_callback_list_calls[3], mock.call().on_train_begin())
            self.assertEqual(mock_callback_list_calls[4], mock.call().on_epoch_begin(1))
            self.assertEqual(mock_callback_list_calls[5], mock.call().on_batch_begin(0, {"batch": 0, "size": 1}))
            self.assertEqual(mock_callback_list_calls[6], mock.call().on_batch_end(0, {"batch": 0, "size": 1}))
            self.assertEqual(mock_callback_list_calls[7], mock.call().on_epoch_end(1, {}))
            self.assertEqual(mock_callback_list_calls[8], mock.call().on_train_end())

            mock_callback_list.return_value.on_batch_begin.assert_called_once()
            mock_callback_list.return_value.on_epoch_end.assert_called_once()

            mock_model.prepare_batch.assert_called_once_with("1", device="cpu")
            mock_model.fit_function.assert_called_once_with((1, 2), mock_loss_fn, mock_optimizer)

    def test_fit_calls_multiple_epochs(self):
        with patch("nn_helper.train.CallbackList") as mock_callback_list:
            mock_model = MagicMock(spec=ModelBase)
            mock_model.prepare_batch.return_value = (1, 2)

            mock_data_loader = MagicMock()
            mock_data_loader.__iter__.return_value = ["1", "2"]
            mock_data_loader.__len__.return_value = 2
            mock_data_loader.batch_size = 1

            mock_loss_fn = MagicMock()
            mock_optimizer = MagicMock()

            fit(
                model=mock_model,
                data_loader=mock_data_loader,
                epochs=2,
                loss_fn=mock_loss_fn,
                optimizer=mock_optimizer,
            )

            # 2 epochs with 2 batches should give 4 prepare_batch calls
            self.assertEqual(len(mock_model.prepare_batch.mock_calls), 4)
            self.assertEqual(len(mock_callback_list.return_value.on_batch_end.mock_calls), 4)

            self.assertEqual(len(mock_callback_list.return_value.on_epoch_end.mock_calls), 2)
            self.assertEqual(len(mock_callback_list.return_value.on_train_end.mock_calls), 1)

    def test_collect_loss_values_in_batch_logs(self):
        with patch("nn_helper.train.CallbackList") as mock_callback_list:
            mock_model = MagicMock(spec=ModelBase)
            mock_model.prepare_batch.return_value = (1, 2)
            mock_model.fit_function.return_value = {"loss": torch.tensor([2.0]), "loss_two": torch.tensor([1.0])}

            mock_data_loader = MagicMock()
            mock_data_loader.__iter__.return_value = ["1"]
            mock_data_loader.__len__.return_value = 1
            mock_data_loader.batch_size = 1

            mock_loss_fn = MagicMock()
            mock_optimizer = MagicMock()

            fit(
                model=mock_model,
                data_loader=mock_data_loader,
                epochs=1,
                loss_fn=mock_loss_fn,
                optimizer=mock_optimizer,
            )

            self.assertEqual(
                mock_callback_list.mock_calls[6],
                mock.call().on_batch_end(0, {"batch": 0, "size": 1, "loss": 2.0, "loss_two": 1.0}),
            )
