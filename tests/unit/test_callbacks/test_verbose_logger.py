import unittest
from unittest.mock import patch

from nn_helper.callbacks.base_callback import CallbackList
from nn_helper.callbacks.verbose_logger import VerboseLogger


class TestVerboseLogger(unittest.TestCase):
    def test_print_on_train_begin(self):
        with patch("nn_helper.callbacks.verbose_logger.print") as mock_print:
            verbose_logger = VerboseLogger()
            verbose_callback = CallbackList([verbose_logger])

            verbose_callback.set_params(params={"metrics": ["random"], "verbose": 1})
            verbose_logger.on_train_begin(logs={})

            mock_print.assert_called_once_with("Begin training...")
            self.assertEqual(verbose_logger.metrics, ["loss", "random"])

    def test_reset_iterations_on_epoch_begin(self):
        verbose_logger = VerboseLogger()
        verbose_callback = CallbackList([verbose_logger])

        verbose_callback.set_params(params={"metrics": ["random"], "verbose": 1})
        verbose_logger.batch_iterations = 1
        verbose_logger.on_epoch_begin(epoch=1, logs={})

        self.assertEqual(verbose_logger.batch_iterations, 0)

    def test_verbose_print_on_batch_begin(self):
        with patch("nn_helper.callbacks.verbose_logger.print") as mock_print:
            verbose_logger = VerboseLogger(log_interval=2)
            verbose_callback = CallbackList([verbose_logger])

            verbose_callback.set_params(params={"metrics": ["random"], "verbose": 2})
            verbose_logger.on_epoch_begin(epoch=1)

            for batch_index in range(2):
                verbose_logger.on_batch_begin(batch_index=batch_index, logs={})

            mock_print.assert_called_with("Batch:", 2)

    def test_verbose_print_on_batch_end(self):
        with patch("nn_helper.callbacks.verbose_logger.print") as mock_print:
            verbose_logger = VerboseLogger(log_interval=2)
            verbose_callback = CallbackList([verbose_logger])

            verbose_callback.set_params(params={"metrics": ["random"], "verbose": 2})
            verbose_logger.on_train_begin()
            verbose_logger.on_epoch_begin(epoch=1)

            for batch_index in range(2):
                verbose_logger.on_batch_end(batch_index=batch_index, logs={"loss": 2})

            mock_print.assert_called_with("loss: 2")

    def test_no_print_for_batch(self):
        with patch("nn_helper.callbacks.verbose_logger.print") as mock_print:
            verbose_logger = VerboseLogger(log_interval=1)
            verbose_callback = CallbackList([verbose_logger])

            verbose_callback.set_params(params={"metrics": ["random"], "verbose": 1})
            verbose_logger.on_train_begin()
            verbose_logger.on_epoch_begin(epoch=1)

            for batch_index in range(2):
                verbose_logger.on_batch_begin(batch_index=batch_index, logs={})
                verbose_logger.on_batch_end(batch_index=batch_index, logs={"loss": 2})

            mock_print.assert_called_once_with("Begin training...")

    def test_verbose_print_on_epoch_end(self):
        with patch("nn_helper.callbacks.verbose_logger.print") as mock_print:
            verbose_logger = VerboseLogger(log_interval=1)
            verbose_callback = CallbackList([verbose_logger])

            verbose_callback.set_params(params={"metrics": ["random"], "verbose": 1})
            verbose_logger.on_train_begin()
            verbose_logger.on_epoch_begin(epoch=1)

            verbose_logger.on_epoch_end(epoch=1, logs={"loss": 2})

            mock_print.assert_any_call("Epoch:", 1)
            mock_print.assert_called_with("loss: 2")

    def test_verbose_print_on_train_end(self):
        with patch("nn_helper.callbacks.verbose_logger.print") as mock_print:
            verbose_logger = VerboseLogger(log_interval=1)
            verbose_callback = CallbackList([verbose_logger])

            verbose_callback.set_params(params={"metrics": ["random"], "verbose": 1})
            verbose_logger.on_train_begin()
            verbose_logger.on_train_end()

            mock_print.assert_called_with("Finished.")
