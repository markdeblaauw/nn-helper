import unittest
from unittest.mock import MagicMock

from nn_helper.callbacks.base_callback import CallbackList
from nn_helper.callbacks.metric_aggregator import MetricAggregator


class TestMetricAggregator(unittest.TestCase):
    def setUp(self) -> None:
        self.metric_aggregator = MetricAggregator()
        self.metric_callback = CallbackList([self.metric_aggregator])

    def test_empty_logs_on_batch_end(self):
        self.metric_callback.set_params(params={"metrics": []})

        self.metric_aggregator.on_epoch_begin(batch_index=1)

        mock_logs = MagicMock()
        mock_logs.__bool__.return_value = False

        self.metric_aggregator.on_batch_end(batch_index=1, logs=mock_logs)

        self.assertEqual(len(mock_logs.mock_calls), 1)

    def test_empty_logs_on_epoch_end(self):
        self.metric_callback.set_params(params={"metrics": []})

        self.metric_aggregator.on_epoch_begin(batch_index=1)

        mock_logs = MagicMock()
        mock_logs.__bool__.return_value = False

        self.metric_aggregator.on_epoch_end(epoch=1, logs=mock_logs)

        self.assertEqual(len(mock_logs.mock_calls), 1)

    def test_if_metrics_are_included_on_batch_end(self):
        self.metric_callback.set_params(params={"metrics": ["random", "dummy_loss"]})

        self.metric_aggregator.on_epoch_begin(batch_index=1)

        self.metric_aggregator.on_batch_end(batch_index=2, logs={"dummy_loss": 10})

        self.assertEqual(self.metric_aggregator.metrics, ["loss", "random", "dummy_loss"])

    def test_on_batch_end_it_stores_logs_values_with_batch_size_one(self):
        self.metric_callback.set_params(params={"metrics": ["dummy_loss"]})

        self.metric_aggregator.on_epoch_begin(batch_index=0)

        for batch_index in range(1, 3):
            self.metric_aggregator.on_batch_end(batch_index=batch_index, logs={"loss": 2, "dummy_loss": 10})

        self.assertEqual(self.metric_aggregator.totals, {"loss": 4, "dummy_loss": 20})
        self.assertEqual(self.metric_aggregator.batches, 2)

    def test_on_batch_end_it_stores_logs_values_with_batch_size_two(self):
        self.metric_callback.set_params(params={"metrics": ["dummy_loss"]})

        self.metric_aggregator.on_epoch_begin(batch_index=0)

        for batch_index in range(1, 3):
            self.metric_aggregator.on_batch_end(batch_index=batch_index, logs={"loss": 2, "dummy_loss": 10, "size": 2})

        self.assertEqual(self.metric_aggregator.totals, {"loss": 8, "dummy_loss": 40})
        self.assertEqual(self.metric_aggregator.batches, 4)

    def test_if_logs_get_available_on_epoch_end(self):
        self.metric_callback.set_params(params={"metrics": ["dummy_loss"]})

        self.metric_aggregator.on_epoch_begin(batch_index=0)

        for batch_index in range(1, 3):
            self.metric_aggregator.on_batch_end(batch_index=batch_index, logs={"loss": 2, "dummy_loss": 10, "size": 2})

        expected_logs = {}
        self.metric_aggregator.on_epoch_end(epoch=1, logs=expected_logs)

        self.assertEqual(expected_logs, {"loss": 2, "dummy_loss": 10})
