import unittest

import torch

from nn_helper.callbacks import LRBatchDecay


class TestLRBatchDecay(unittest.TestCase):
    def test_correct_learning_rate_adjustmen(self):
        model = [torch.nn.parameter.Parameter(torch.randn(2, 2, requires_grad=True))]
        optimizer = torch.optim.SGD(model, 1.0)

        lr_scheduler = LRBatchDecay(lr=1.0, lr_decay=1e-1)
        lr_scheduler.set_params(
            {
                "optimizer": optimizer,
            }
        )

        for i in range(1000):
            lr_scheduler.on_batch_begin(batch_index=i, logs={})

        for param_group in optimizer.param_groups:
            self.assertAlmostEqual(param_group["lr"], 0.00990099, places=8)
