import unittest
from unittest.mock import MagicMock

from nn_helper.callbacks.base_callback import BaseCallback, CallbackList


class TestBaseCallback(unittest.TestCase):
    def test_if_every_callback_from_callback_list_is_called(self):
        mock_callback = MagicMock()

        callbacks = CallbackList(callbacks=[mock_callback])

        callbacks.set_params({"number": 1})
        callbacks.set_model(model="empty")
        callbacks.on_train_begin()
        callbacks.on_epoch_begin(epoch=1, logs={"string": "dum"})
        callbacks.on_batch_begin(batch_index=1)
        callbacks.on_batch_end(batch_index=1)
        callbacks.on_epoch_end(epoch=1)
        callbacks.on_train_end()

        mock_callback.set_params.assert_called_once_with({"number": 1})
        mock_callback.set_model.assert_called_once_with("empty")
        mock_callback.on_train_begin.assert_called_once_with({})
        mock_callback.on_epoch_begin.assert_called_once_with(1, {"string": "dum"})
        mock_callback.on_batch_begin.assert_called_once_with(1, {})
        mock_callback.on_batch_end.assert_called_once_with(1, {})
        mock_callback.on_epoch_end.assert_called_once_with(1, {})
        mock_callback.on_train_end.assert_called_once_with({})

    def test_if_methods_from_base_callback_are_called(self):
        class DummyCallback(BaseCallback):
            def __init__(self):
                super().__init__()

        dummy_callback = DummyCallback()

        callbacks = CallbackList([dummy_callback])

        callbacks.set_params({"number": 1})
        callbacks.set_model(model="empty")
        callbacks.on_train_begin()
        callbacks.on_epoch_begin(epoch=1, logs={"string": "dum"})
        callbacks.on_batch_begin(batch_index=1)
        callbacks.on_batch_end(batch_index=1)
        callbacks.on_epoch_end(epoch=1)
        callbacks.on_train_end()

        self.assertEqual(dummy_callback.params, {"number": 1})
        self.assertEqual(dummy_callback.model, "empty")
