import os
import unittest
from tempfile import TemporaryDirectory

import torch

import nn_helper.callbacks as callbacks
from tests.test_model.model import LinearRegression


class TestSaveModel(unittest.TestCase):
    def setUp(self) -> None:
        # create model
        self.lr_model = LinearRegression()
        self.lr_optimizer = torch.optim.Adam(params=self.lr_model.parameters(), lr=1.0)

    def test_save_checkpoint_deactivated(self):
        with TemporaryDirectory() as tmp_directory:
            save_model = callbacks.SaveModel(model_path=tmp_directory)
            save_model.set_model(model=self.lr_model)
            save_model.set_params(
                params={
                    "optimizer": self.lr_optimizer,
                    "num_batches": 10,
                    "batch_size": 2,
                    "metrics": [],
                    "verbose": 1,
                }
            )

            save_model.on_epoch_end(epoch=11, logs={})

            self.assertFalse("model_checkpoint_epoch=11.pth" in os.listdir(tmp_directory))

    def test_save_checkpoint_model(self):
        with TemporaryDirectory() as tmp_model, TemporaryDirectory() as tmp_checkpoint:
            save_model = callbacks.SaveModel(model_path=tmp_model, checkpoint_path=tmp_checkpoint)
            save_model.set_model(model=self.lr_model)
            save_model.set_params(
                params={
                    "optimizer": self.lr_optimizer,
                    "num_batches": 10,
                    "batch_size": 2,
                    "metrics": [],
                    "verbose": 1,
                }
            )

            save_model.on_epoch_end(epoch=11, logs={})

            self.assertTrue("model_checkpoint_epoch=11.pth" in os.listdir(tmp_checkpoint))

            new_lr_model = LinearRegression()
            new_lr_optimizer = torch.optim.Adam(params=self.lr_model.parameters(), lr=1.0)

            checkpoint = torch.load(os.path.join(tmp_checkpoint, "model_checkpoint_epoch=11.pth"))
            self.assertEqual(
                [key for key in checkpoint.keys()], ["epoch", "batch_count", "model_state_dict", "optimizer_state_dict"]
            )

            new_lr_model.load_state_dict(state_dict=checkpoint["model_state_dict"])
            new_lr_optimizer.load_state_dict(checkpoint["optimizer_state_dict"])

            self.assertEqual(checkpoint["epoch"], 11)
            self.assertEqual(checkpoint["batch_count"], 110)

    def test_save_checkpoint_submodels(self):
        with TemporaryDirectory() as tmp_model, TemporaryDirectory() as tmp_checkpoint:
            save_model = callbacks.SaveModel(
                model_path=tmp_model,
                checkpoint_path=tmp_checkpoint,
                submodels={"linear_1": self.lr_model.linear_1, "linear_2": self.lr_model.linear_2},
            )
            save_model.set_model(model=self.lr_model)
            save_model.set_params(
                params={
                    "optimizer": self.lr_optimizer,
                    "num_batches": 10,
                    "batch_size": 2,
                    "metrics": [],
                    "verbose": 1,
                }
            )

            save_model.on_epoch_end(epoch=11, logs={})

            self.assertTrue("model_checkpoint_epoch=11.pth" in os.listdir(tmp_checkpoint))

            new_lr_model = LinearRegression()
            new_lr_optimizer = torch.optim.Adam(params=self.lr_model.parameters(), lr=1.0)

            checkpoint = torch.load(os.path.join(tmp_checkpoint, "model_checkpoint_epoch=11.pth"))
            self.assertEqual(
                [key for key in checkpoint.keys()],
                ["epoch", "batch_count", "optimizer_state_dict", "linear_1_state_dict", "linear_2_state_dict"],
            )

            new_lr_model.linear_1.load_state_dict(state_dict=checkpoint["linear_1_state_dict"])
            new_lr_model.linear_2.load_state_dict(state_dict=checkpoint["linear_2_state_dict"])
            new_lr_optimizer.load_state_dict(checkpoint["optimizer_state_dict"])

            self.assertEqual(checkpoint["epoch"], 11)
            self.assertEqual(checkpoint["batch_count"], 110)

    def test_save_model(self):
        with TemporaryDirectory() as tmp_model:
            save_model = callbacks.SaveModel(model_path=tmp_model)
            save_model.set_model(model=self.lr_model)
            save_model.set_params(
                params={
                    "optimizer": self.lr_optimizer,
                    "num_batches": 10,
                    "batch_size": 2,
                    "metrics": [],
                    "verbose": 1,
                }
            )

            save_model.on_train_end(logs={})

            self.assertTrue("model.pth" in os.listdir(tmp_model))

    def test_save_submodels(self):
        with TemporaryDirectory() as tmp_directory:
            save_model = callbacks.SaveModel(
                model_path=tmp_directory,
                submodels={"linear_1": self.lr_model.linear_1, "linear_2": self.lr_model.linear_2},
            )
            save_model.set_model(model=self.lr_model)
            save_model.set_params(
                params={
                    "optimizer": self.lr_optimizer,
                    "num_batches": 10,
                    "batch_size": 2,
                    "metrics": [],
                    "verbose": 1,
                }
            )

            save_model.on_train_end(logs={})

            self.assertTrue("linear_1.pth" in os.listdir(tmp_directory))
            self.assertTrue("linear_2.pth" in os.listdir(tmp_directory))
