import unittest

import torch

from nn_helper.base.model import ModelBase


class SimpleLinearModel(ModelBase):
    def __init__(self):
        super().__init__()
        self.linear = torch.nn.Linear(3, 1)
        self.flatten = torch.nn.Flatten(0, 1)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.linear(x)
        return self.flatten(x)


class LossFunction(torch.nn.Module):
    def __init__(self) -> None:
        super().__init__()
        self.loss_fn = torch.nn.MSELoss()

    def forward(self, x: torch.Tensor, y: torch.Tensor) -> float:
        mse_loss = self.loss_fn(x, y)
        just_a_number = 1.0

        return {"mse_loss": mse_loss, "number_loss": just_a_number}


class TestModel(unittest.TestCase):
    def test_model_base_abstract_forward_function(self):
        class SimpleModel(ModelBase):
            def __init__(self):
                super().__init__()

        self.assertRaises(TypeError, SimpleModel)

    def test_prepare_batch(self):
        model = SimpleLinearModel()

        x, y = model.prepare_batch(batch=(torch.rand([2, 2]), torch.rand([2, 2])))

        self.assertIsInstance(x, torch.Tensor)
        self.assertIsInstance(y, torch.Tensor)

    def test_fit_function_single_loss(self):
        class LossFunction(torch.nn.Module):
            def __init__(self) -> None:
                super().__init__()
                self.loss_fn = torch.nn.MSELoss()

            def forward(self, x: torch.Tensor, y: torch.Tensor) -> float:
                return self.loss_fn(x, y)

        model = SimpleLinearModel()
        optimizer = torch.optim.Adam(params=model.parameters(), lr=1e-2)

        output = model.fit_function(
            input_batch=(torch.rand([1, 3]), torch.rand([1])), loss_fn=LossFunction(), optimizer=optimizer
        )

        self.assertIsInstance(output["loss"], torch.Tensor)
        self.assertTrue(len(output), 1)

    def test_fit_function_double_loss(self):
        class LossFunction(torch.nn.Module):
            def __init__(self) -> None:
                super().__init__()
                self.loss_fn = torch.nn.MSELoss()

            def forward(self, x: torch.Tensor, y: torch.Tensor) -> float:
                mse_loss = self.loss_fn(x, y)
                just_a_number = torch.rand([1])

                return {"loss": mse_loss, "number_loss": just_a_number}

        model = SimpleLinearModel()
        optimizer = torch.optim.Adam(params=model.parameters(), lr=1e-2)

        output = model.fit_function(
            input_batch=(torch.rand([1, 3]), torch.rand([1])), loss_fn=LossFunction(), optimizer=optimizer
        )

        self.assertTrue(len(output), 2)
        self.assertIsInstance(output["loss"], torch.Tensor)
        self.assertIsInstance(output["number_loss"], torch.Tensor)
