"""Package that contains base classes for nn_helper."""
from nn_helper.base.model import ModelBase


__all__ = ["ModelBase"]
